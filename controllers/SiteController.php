<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Equipo;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud() {
        return $this->render("gestion");
    }
    
    public function actionConsulta0a(){
        $dataProvider=new ActiveDataProvider([
            'query'=>Ciclista::find(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>[],
            "titulo"=>"Consulta 0 con Active Record",
            "enunciado"=>"Listar todos los campos y registros de ciclistas",
            "sql"=>"select * from ciclista",
        ]);
    }
    
    public function actionConsulta0(){
       
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
       
        $dataProvider=new SqlDataProvider([
            'sql'=>'select * from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>[],
            "titulo"=> "Consulta 0 sin Active Record",
            "enunciado"=>"Listar todos los campos y registros de ciclistas ",
            "sql"=>"select * from ciclista",
        ]);
    }
    
    
    
    
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta1() {
        // mediante DAO = Objeto de Acceso a Datos
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 sin Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
    public function actionConsulta2() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
     public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach' or nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"G_avg(edad)  (σ_(nomequipo='Banesto')(ciclista)",
        ]);
    }
    
    public function actionConsulta4a(){
        //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("dorsal")
                ->Where(['or',['<','edad',25],['>','edad',30]]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad<25 OR edad>30",
        ]);
    }
    
    public function actionConsulta4(){
        
        $total=Yii::$app->db->createCommand(
                "select count(dorsal) from ciclista where edad<25 or edad>30")               
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad<25 OR edad>30',
            'totalCount'=>$total,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 4 sin Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad<25 OR edad>30",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("dorsal")
                ->Where(['and',['between','edad', 28,32], ['nomequipo'=> 'Banesto']]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto",
            "sql"=>"select dorsal from ciclista where edad between 28 and 32 AND nomequipo='Banesto' ",
        ]);
    }
    
    public function actionConsulta5(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("dorsal, nomequipo")
                ->where(['and',['between','edad', 28,32], ['nomequipo'=> 'Banesto']]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
         return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nomequipo0.director'],
            "titulo"=> "Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales y el nombre del director cuyos ciclistas tienen una edad entre 28 y 32 y además que solo sean de Banesto",
            "sql"=>"select dorsal, director from ciclista join equipo using (nomequipo) where edad between 28 and 32 AND nomequipo='Banesto'  ",
        ]);
    }
    
    public function actionConsulta6a() {
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nombre")
                ->where("CHAR_LENGTH(nombre)>8"),
            'pagination'
        ]);
    }
    
    /**
     * 
     * alguien me pide informacion
     */
    public function actionEnviar(){
        Yii::$app->mailer->compose()
                ->setTo("cintiadiazar@gmail.com")
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo(["cliente@gmail.com" => "Andres lopez"]) // formulario de peticion
                ->setSubject("pedir informacion sobre algo")
                ->setTextBody("Quiero mas informacion sobre bolis")                
                ->send();
    }
    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        /* cargar el use contactform */
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            //var_dump($model);
            // contact es la funcion y guardo el correo
            $model->contact(Yii::$app->params['informacion']);

            //return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
}
