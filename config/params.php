<?php

return [
    'informacion' => 'cintiadiazar@gmail.com', // solicitud correo de informacion
    'contacto' => 'cintiadiazar@gmail.com', //solicitud de correo de contacto    
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'responder' => 'empresa@alpe.es', // direccion a quién responder
];
