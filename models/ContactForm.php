<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $nombre;
    public $email;
    public $tema;
    public $descripcion;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['nombre', 'email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['nombre','string', 'max'=>20],
            // [['nombre','apellidos'], 'safe'] asignacion masiva para meterlo en bd de una tirada
            [['descripcion','tema'], 'safe']
            // guardo tema y descripcion
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Escribe tu nombre',
            'email' => 'Escribe el correo electronico de contacto',
            'tema' => 'Sobre qué producto quieres informacion',
            'descripcion' => 'Indica un poco más qué informacion necesitas'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    
    
    public function contact($email)
    {
        // validate es para validar las rules
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->nombre])
                ->setSubject($this->tema)
                ->setTextBody($this->descripcion)
                ->send();

            return true;
        }
        return false;
    }
}
